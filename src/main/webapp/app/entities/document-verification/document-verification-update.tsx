import React, { useState, useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, FormText,Modal, ModalHeader, ModalBody } from 'reactstrap';
import { isNumber, Translate, translate, ValidatedField, ValidatedForm } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';


import { useAppDispatch, useAppSelector } from 'app/config/store';

import { DocumentType } from 'app/shared/model/enumerations/document-type.model';
import {  createEntity, reset } from './document-verification.reducer';
import LoadingBar from "react-redux-loading-bar";

export const DocumentVerificationUpdate = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  const [isNew] = useState(!props.match.params || !props.match.params.id);
  const [translationLoaded, setTranslationLoaded] = useState(false);

  const documentVerificationEntity = useAppSelector(state => state.documentVerification.entity);
  const showModal = useAppSelector(state => state.documentVerification.showModel);
  const updateSuccess = useAppSelector(state => state.documentVerification.errorMessage);
  const languageLoaded = useAppSelector(state => state.locale.loaded);

  const loading = useAppSelector(state => state.documentVerification.loading);
  const updating = useAppSelector(state => state.documentVerification.updating);
  const documentTypeValues = Object.keys(DocumentType);
  const handleClose = () => {
    props.history.push('/document-verification/new');
  };

  useEffect(() => {
      dispatch(reset());
  }, []);

  useEffect(() => {
    if (updateSuccess) {
      handleClose();
    }
  }, [updateSuccess]);

  const saveEntity = values => {
    const entity = {
      ...documentVerificationEntity,
      ...values,
    };

    dispatch(createEntity(entity));
  };


  const defaultValues = () =>
    isNew
      ? {}
      : {
          documentType: 'IBAN_LETTER',
          ...documentVerificationEntity,
        };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="documentVerificationApp.documentVerification.home.createOrEditLabel" data-cy="DocumentVerificationCreateUpdateHeading">
            <Translate contentKey="documentVerificationApp.documentVerification.home.createOrEditLabel">
              Create or edit a DocumentVerification
            </Translate>
          </h2>
        </Col>
      </Row>

      <Modal isOpen = {showModal} size="lg"  toggle={function noRefCheck(){dispatch(reset())}}
             centered >
        <ModalHeader  toggle={function noRefCheck(){dispatch(reset())}} data-cy="documentVerificationDeleteDialogHeading">
          {updateSuccess ? translate('documentVerificationApp.documentVerification.documentIsValid') : translate('documentVerificationApp.documentVerification.documentIsNotValid')}
        </ModalHeader>
        <ModalBody>
          {updateSuccess ? translate('documentVerificationApp.documentVerification.documentIsValid') : translate('documentVerificationApp.documentVerification.documentIsNotValid')}
        </ModalBody>
      </Modal>



      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <ValidatedForm defaultValues={defaultValues()} onSubmit={saveEntity}>

              <ValidatedField
                label={translate('documentVerificationApp.documentVerification.documentType')}
                id="document-verification-documentType"
                name="documentType"
                data-cy="documentType"
                type="select"
              >
                {documentTypeValues.map(documentType => (
                  <option value={documentType} key={documentType}>
                    {translate('documentVerificationApp.DocumentType.' + documentType)}
                  </option>
                ))}
              </ValidatedField>


              <ValidatedField
                name="customerNumber"
                label={translate('documentVerificationApp.documentVerification.customerNumber')}
                id="document-verification-customerNumber"
                validate={{
                  required: { value: true, message: translate('documentVerificationApp.documentVerification.messages.validate.customerNumber.required') },
                  pattern: {
                    value: /^[a-zA-Z0-9!$&*+=?^_`{|}~.-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$|^[_.@A-Za-z0-9-]+$/,
                    message: translate('documentVerificationApp.documentVerification.messages.validate.customerNumber.pattern'),
                  },
                  minLength: { value: 1, message: translate('documentVerificationApp.documentVerification.messages.validate.customerNumber.minlength') },
                  maxLength: { value: 20, message: translate('documentVerificationApp.documentVerification.messages.validate.customerNumber.maxlength') },
                }}
                data-cy="customerNumber"
              />

              <ValidatedField
                name="referenceNumber"
                label={translate('documentVerificationApp.documentVerification.referenceNumber')}
                id="document-verification-referenceNumber"
                validate={{
                  required: { value: true, message: translate('documentVerificationApp.documentVerification.messages.validate.referenceNumber.required') },
                  pattern: {
                    value: /^[a-zA-Z0-9!$&*+=?^_`{|}~.-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$|^[_.@A-Za-z0-9-]+$/,
                    message: translate('documentVerificationApp.documentVerification.messages.validate.referenceNumber.pattern'),
                  },
                  minLength: { value: 1, message: translate('documentVerificationApp.documentVerification.messages.validate.referenceNumber.minlength') },
                  maxLength: { value: 20, message: translate('documentVerificationApp.documentVerification.messages.validate.referenceNumber.maxlength') },
                }}
                data-cy="referenceNumber"
              />

              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="documentVerificationApp.documentVerification.verifyButtonLabel">Save</Translate>
              </Button>
            </ValidatedForm>

          )}
        </Col>
      </Row>
    </div>
  );
};

export default DocumentVerificationUpdate;
