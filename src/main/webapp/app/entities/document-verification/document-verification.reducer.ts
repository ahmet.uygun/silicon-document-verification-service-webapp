import axios from 'axios';
import {createAsyncThunk, isFulfilled, isPending, isRejected} from '@reduxjs/toolkit';

import {cleanEntity} from 'app/shared/util/entity-utils';
import {IQueryParams, createEntitySlice, EntityState, serializeAxiosError} from 'app/shared/reducers/reducer.utils';
import {IDocumentVerification, defaultValue} from 'app/shared/model/document-verification.model';

const initialState: EntityState<IDocumentVerification> = {
  loading: false,
  errorMessage: null,
  entities: [],
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
  showModel: false,

};

const apiUrl = '/v2/api/documents/verify';


export const createEntity = createAsyncThunk(
  'documentVerification/create_entity',
  async (entity: IDocumentVerification, thunkAPI) => {
    const result = await axios.post<IDocumentVerification>(apiUrl, cleanEntity(entity));
    return result;
  },
  {serializeError: serializeAxiosError}
);
export const DocumentVerificationSlice = createEntitySlice({
  name: 'documentVerification',
  initialState,
  extraReducers (builder) {
    builder
      .addMatcher(isFulfilled(createEntity), (state, action) => {
        state.updating = false;
        state.loading = false;
        state.updateSuccess = true;
        state.entity = action.payload.data;
        state.showModel = true
      })
      .addMatcher(isPending(createEntity), state => {
        state.errorMessage = null;
        state.updateSuccess = false;
        state.updating = true;
        state.showModel = false
      })
      .addMatcher(isRejected(createEntity), (state, action)  => {
        // eslint-disable-next-line no-debugger
        debugger;
        state.errorMessage = null;
        state.updateSuccess = false;
        state.updating = true;
        state.showModel = true;
      });
  },
});

export const {reset} = DocumentVerificationSlice.actions;

// Reducer
export default DocumentVerificationSlice.reducer;
