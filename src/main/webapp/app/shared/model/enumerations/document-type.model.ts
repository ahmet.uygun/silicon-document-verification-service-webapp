export enum DocumentType {
  IBAN_LETTER = 'Iban Letter',

  BALANCE_CERT = 'Balance Certification',

  STATEMENT = 'Statement',
}
