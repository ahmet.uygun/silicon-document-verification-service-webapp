import { DocumentType } from 'app/shared/model/enumerations/document-type.model';

export interface IDocumentVerification {
  id?: number;
  documentType?: DocumentType | null;
  customerNumber?: string | null;
  referenceNumber?: string | null;
}

export const defaultValue: Readonly<IDocumentVerification> = {};
