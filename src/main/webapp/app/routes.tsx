import React from 'react';
import { Switch } from 'react-router-dom';
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';
import PageNotFound from 'app/shared/error/page-not-found';
import DocumentVerificationUpdate from "app/entities/document-verification/document-verification-update";


const Routes = () => {
  return (
    <div className="view-routes">
      <Switch>

        <ErrorBoundaryRoute path="/" component={DocumentVerificationUpdate} />
        <ErrorBoundaryRoute component={PageNotFound} />
      </Switch>
    </div>
  );
};

export default Routes;
